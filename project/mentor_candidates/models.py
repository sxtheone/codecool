from django.db import models


class Mentor(models.Model):
    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField()
    description = models.TextField(blank=True)


class Opinion(models.Model):
    def __str__(self):
        return 'Student: %s, Mentor: %s %s' % (self.name, self.mentor.first_name, self.mentor.last_name)
    mentor = models.ForeignKey(Mentor, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    description = models.TextField()
