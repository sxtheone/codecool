from django.contrib import admin
from .models import Mentor
from .models import Opinion


admin.site.register(Mentor)
admin.site.register(Opinion)
